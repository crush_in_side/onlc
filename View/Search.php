<?php
/**
 * User: Rashit Hamidullin Rashit.Hamidullin@gmail.com
 * Date: 25.05.14
 * Time: 11:34
 * 
 * 
 */
?>
<div class="starter-template">
    <h2>Введите поисковую строку</h2>
</div>
<div class="row">
    <div class="col-md-6 col-md-offset-3">
        <form action="index.php" method="POST">
        <div class="input-group">
                <input type="text" name="find" class="form-control">
                <span class="input-group-btn">
                    <button class="btn btn-primary" type="submit">Искать.</button>
                </span>
        </div><!-- /input-group -->
        </form>
    </div><!-- /.col-lg-6 -->
</div><!-- /.row -->
<?php if(is_null($goods) === false){?>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-results">
                <?php foreach($goods as $good){?>
                    <tr class="source_type_<?=$good['source']?>">
                        <td><?=$good['id']?></td>
                        <td><?=$good['name']?></td>
                        <td><?=number_format($good['price'], 2, '.', ' ')?> <s>Р</s></td>
                        <td><?=$inStockEnum[$good['in_stock']]?></td>
                        <td><?=$good['company_name']?></td>
                        <td><?=$SourceEnum[$good['source']]?></td>
                    </tr>
                <?php };?>
            </table>
        </div>
    </div>
<?php };?>