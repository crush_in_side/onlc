<?php
include_once('Model/M_SQL.php');

$connect = MSQL::Instance();

echo 'Соединение успешно установлено... ' . $connect->mysqli->host_info . "<br />";

echo 'Загрузка файла дампа' . "<br />";

$sql = file_get_contents('dump/dump.sql');

if (!$sql){
    die ('Ошибка открытия файла дампа.');
}

echo 'Загрузка дампа в базу <br />';

$connect->mysqli->multi_query($sql);

echo 'Готово. Можно приступать к <a href="index.php">поиску</a>.';

