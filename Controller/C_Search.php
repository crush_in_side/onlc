<?php
/**
 * User: Rashit Hamidullin Rashit.Hamidullin@gmail.com
 * Date: 25.05.14
 * Time: 10:57
 */
include_once ('Controller/C_Page.php');
include_once('Model/M_Goods.php');
include_once('Enum/InStock.php');
include_once('Enum/Source.php');

class C_Search extends C_Page
{
    private $Goods;
    private $findedGoods;
    private $formErrors;

    protected function OnInput()
    {
        parent::OnInput();

        $this->findedGoods = null;

        $this->Goods = new M_Goods();

        $this->title = 'Поиск';

        if ($this->isPost()) {
            $searchString = $_POST['find'];

            $trimmedString = trim($searchString);

            if (strlen($trimmedString) > 0) {
                $this->findedGoods = $this->Goods->findGoods($trimmedString);
            }
        }
    }

    protected function OnOutput()
    {
        $vars = array('goods' => $this->findedGoods);

        if (is_null($this->findedGoods) === false) {
            $inStockEnum = new InStock();
            $SourceEnum = new Source();
            $vars['inStockEnum'] = $inStockEnum->list;
            $vars['SourceEnum'] = $SourceEnum->list;

        }

        $this->content = $this->Template('View/Search.php', $vars);
        parent::OnOutput();
    }
}