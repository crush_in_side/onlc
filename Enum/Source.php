<?php
/**
 * User: Rashit Hamidullin Rashit.Hamidullin@gmail.com
 * Date: 25.05.14
 * Time: 16:30
 * 
 * 
 */
include_once ('Reference.php');

class Source extends Reference
{
    const PRICE = 0;
    const TENDER = 1;
    const SHOP = 2;

    public function __construct()
    {
        $this->list = array(
            self::PRICE => 'прайс-лист',
            self::TENDER => 'прошлые тендеры',
            self::SHOP => 'интернет-магазин'
        );
    }

}